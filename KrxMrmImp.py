from .file import TFile
from .gui import call_gui, call_message_box
from .helpers import CallUIType, TZENArchive
from .material import link_texture_to_material, new_material
from .mesh import MeshData, new_mesh_object
from .scene import reset_scene
from .system import load_temp_json_file


class TMRMFileLoader:
    __slots__ = (
        "__scale_coef",
        "__file",
        "__filename",
        "__imported_materials",
        "__imp_objects",
        "__cur_obj",
        "__cur_name_in_file",
        "__cur_mesh",
        "__mrm_version",
        "__data_pos",
        "__data_size",
        "__num_sub_meshes",
        "__pos_sub_meshes",
        "__pos_verts",
        "__num_verts",
        "__pos_materials",
    )

    def __init__(self):
        self.__scale_coef = 1.0
        self.__file = TFile()
        self.__filename = ""
        self.__imported_materials = []
        self.__imp_objects = []
        self.__cur_obj = None
        self.__cur_name_in_file = ""
        self.__cur_mesh: MeshData = None
        self.__mrm_version = 0
        self.__data_pos = 0
        self.__data_size = 0
        self.__num_sub_meshes = 0
        self.__pos_sub_meshes = 0
        self.__pos_verts = 0
        self.__num_verts = 0
        self.__pos_materials = 0

    def __iter__(self):
        yield "scale_coefficient", self.__scale_coef

    def __create_object(self):
        self.__cur_name_in_file = "zengin"
        self.__cur_obj = new_mesh_object(self.__cur_name_in_file)
        self.__cur_mesh = MeshData(self.__cur_obj)
        self.__imp_objects.append(self.__cur_obj)

    def __read_vertices(self):
        self.__cur_mesh.verts = []
        self.__file.SetPos(self.__pos_verts)
        pattern = "3f" * self.__num_verts
        verts_pos = self.__file.ReadData(pattern, self.__num_verts * 12)
        for i in range(0, len(verts_pos), 3):
            x, z, y = verts_pos[i : i + 3]
            x *= self.__scale_coef
            z *= self.__scale_coef
            y *= self.__scale_coef
            self.__cur_mesh.verts.append([x, y, z])

    def __read_uv_mapping(self):
        self.__cur_mesh.tverts = []

        for i in range(self.__num_sub_meshes):
            self.__file.SetPos(self.__pos_sub_meshes + i * 80 + 8)
            submesh_stats = self.__file.ReadData("ll", 8)
            pos_wdg_sub_mesh = submesh_stats[0] + self.__data_pos
            num_wdg_sub_mesh = submesh_stats[1]
            for j in range(num_wdg_sub_mesh):
                self.__file.SetPos(pos_wdg_sub_mesh + j * 24 + 12)
                u, v = self.__file.ReadData("ff", 8)
                self.__cur_mesh.tverts.append([u, -v])

    def __read_faces(self):
        self.__cur_mesh.faces = []
        self.__cur_mesh.tvfaces = []
        t_vert_base = 0
        num_verts = len(self.__cur_mesh.verts)
        num_t_verts = len(self.__cur_mesh.tverts)

        for i in range(self.__num_sub_meshes):
            self.__file.SetPos(self.__pos_sub_meshes + i * 80 + 0)
            face_data = self.__file.ReadData("4l", 16)

            pos_tri_sub_mesh = face_data[0] + self.__data_pos
            num_tri_sub_mesh = face_data[1]
            pos_wdg_sub_mesh = face_data[2] + self.__data_pos
            num_wdg_sub_mesh = face_data[3]

            for j in range(num_tri_sub_mesh):
                self.__file.SetPos(pos_tri_sub_mesh + j * 6 + 0)
                wdg_idx_0 = self.__file.ReadUnsignedShort()
                self.__file.SetPos(pos_wdg_sub_mesh + wdg_idx_0 * 24 + 20)
                vert_idx_0 = self.__file.ReadUnsignedShort()
                t_vert_idx_0 = wdg_idx_0 + t_vert_base

                self.__file.SetPos(pos_tri_sub_mesh + j * 6 + 2)
                wdg_idx_1 = self.__file.ReadUnsignedShort()
                self.__file.SetPos(pos_wdg_sub_mesh + wdg_idx_1 * 24 + 20)
                vert_idx_1 = self.__file.ReadUnsignedShort()
                t_vert_idx_1 = wdg_idx_1 + t_vert_base

                self.__file.SetPos(pos_tri_sub_mesh + j * 6 + 4)
                wdg_idx_2 = self.__file.ReadUnsignedShort()
                self.__file.SetPos(pos_wdg_sub_mesh + wdg_idx_2 * 24 + 20)
                vert_idx_2 = self.__file.ReadUnsignedShort()
                t_vert_idx_2 = wdg_idx_2 + t_vert_base

                vert_err_idx = None
                if vert_idx_0 >= num_verts:
                    vert_err_idx = vert_idx_0

                if vert_idx_1 >= num_verts and not vert_err_idx:
                    vert_err_idx = vert_idx_1

                if vert_idx_2 >= num_verts and not vert_err_idx:
                    vert_err_idx = vert_idx_2

                if vert_err_idx:
                    raise RuntimeError(
                        f"{__name__}: "
                        f"Vertex index is out of range while reading multi-resolution mesh.\n"
                        f"Vertex index: {vert_err_idx} "
                        f"(Allowable range: 0..{num_verts - 1}).\n"
                        f'File name: "{self.__file.GetName()}".'
                    )

                t_vert_err_idx = None
                if t_vert_idx_0 >= num_t_verts:
                    t_vert_err_idx = t_vert_idx_0

                if t_vert_idx_1 >= num_t_verts:
                    t_vert_err_idx = t_vert_idx_1

                if t_vert_idx_2 >= num_t_verts:
                    t_vert_err_idx = t_vert_idx_2

                if t_vert_err_idx:
                    raise RuntimeError(
                        f"{__name__}: "
                        f"Texture vertex index is out of range while reading multi-resolution mesh.\n"
                        f"Texture vertex index: {t_vert_err_idx} "
                        f"(Allowable range: 0..{num_t_verts - 1}).\n"
                        f'File name: "{self.__file.GetName()}".'
                    )

                self.__cur_mesh.faces.append([vert_idx_0, vert_idx_1, vert_idx_2])
                self.__cur_mesh.tvfaces.append(
                    (t_vert_idx_0, t_vert_idx_1, t_vert_idx_2)
                )
                self.__cur_mesh.face_materials.append(self.__imported_materials[i])

            t_vert_base += num_wdg_sub_mesh

    def __read_materials(self, color_adjustment=None):
        self.__file.SetPos(self.__pos_materials)
        zen_archive = TZENArchive()
        zen_archive.ReadHeader(self.__file)
        for i in range(self.__num_sub_meshes):
            zen_archive.ReadString(self.__file)  # name
            pos = self.__file.GetPos()
            zen_chunk = zen_archive.ReadChunkStart(self.__file)
            if zen_chunk.class_name != "zCMaterial":
                raise RuntimeError(
                    f"{__name__}: "
                    f'A chunk of class "zCMaterial" expected here.\n'
                    f'Position: {"0x"+hex(pos)[2:].upper()}.\n'
                    f'File name: "{self.__file.GetName()}".'
                )

            name = self.__file.ReadString()
            material_stats = self.__file.ReadData("5Bf", 9)
            blue = material_stats[1] / 255.0
            green = material_stats[2] / 255.0
            red = material_stats[3] / 255.0
            texture = self.__file.ReadString()
            zen_archive.ReadChunkEnd(self.__file, zen_chunk)

            material = new_material(name)
            self.__imported_materials.append(material)
            material.diffuse_color = (red, green, blue, 1)
            link_texture_to_material(
                material,
                texture,
                import_file=self.__filename,
                color_adjustment=color_adjustment,
            )

    def __read_mrm_data(self, color_adjustment=None):
        self.__mrm_version = self.__file.ReadUnsignedShort()
        self.__data_size = self.__file.ReadUnsignedLong()
        self.__data_pos = self.__file.GetPos()
        self.__file.SetPos(self.__data_pos + self.__data_size)
        self.__num_sub_meshes = self.__file.ReadUnsignedChar()

        self.__pos_verts = self.__file.ReadUnsignedLong() + self.__data_pos
        self.__num_verts = self.__file.ReadUnsignedLong()
        self.__file.SkipByOffset(8)
        # _ = self.__file.ReadUnsignedLong() + self.__data_pos  # pos_t_axes
        # _ = self.__file.ReadUnsignedLong()  # num_t_axes
        self.__pos_sub_meshes = self.__file.GetPos()
        self.__pos_materials = self.__pos_sub_meshes + 80 * self.__num_sub_meshes

        self.__read_materials(color_adjustment)
        self.__create_object()
        self.__read_vertices()
        self.__read_uv_mapping()
        self.__read_faces()

    def ReadMRMFile(
        self,
        filename,
        space_transform,
        remove_sectored_materials=False,
        color_adjustment=None,
    ):
        self.__init__()
        self.__filename = filename
        self.__scale_coef = space_transform
        try:
            self.__file.Open(filename, "rb")
            file_beginning = True
            while not self.__file.Eof():
                chunk_type = self.__file.ReadUnsignedShort()
                if file_beginning and chunk_type != 0xB100:
                    raise RuntimeError(
                        f'{__name__}: File is not a multi-resolution mesh.\nFile name: "{self.__file.GetName()}".'
                    )

                file_beginning = False
                file_size = self.__file.ReadUnsignedLong()
                chunk_pos = self.__file.GetPos()
                if chunk_type == 0xB100:
                    self.__read_mrm_data(color_adjustment)
                elif chunk_type == 0xB1FF:
                    break

                self.__file.SetPos(chunk_pos + file_size)

            self.__file.Close()
            self.__cur_mesh.update(remove_sectored_materials=remove_sectored_materials)

        except RuntimeError as err:
            self.__file.Close()
            raise err


# Only for internal use by KrxImportExportManager function
def KrxMrmImpGUI(filename: str):
    ui_messages = call_gui(filename, CallUIType.IMPORT_MRM)

    if ui_messages is None:
        return None
    elif "RESTART_GUI" in ui_messages:
        KrxMrmImpGUI(filename)
        return None
    elif "SUCCESS" in ui_messages:
        ui_data_result = load_temp_json_file(tool="OUTPUT_IMPORT")

        if not ui_data_result:
            call_message_box(error_message="Code -1: Invalid JSON data!")
            return None

        if ui_data_result["scene_mode"] == 1:
            reset_scene()

        TMRMFileLoader().ReadMRMFile(
            filename,
            ui_data_result["scale"],
            ui_data_result["remove_sectored_materials"],
            ui_data_result["color_adjustment"],
        )


# For calling outside KrxImpExp module
def KrxMrmImp(
    filename: str,
    scale: float = 0.01,
    remove_sectored_materials: bool = True,
    color_adjustment: float = None
):
    TMRMFileLoader().ReadMRMFile(
        filename, scale, remove_sectored_materials, color_adjustment=color_adjustment
    )
