# impexp.py: Helper utilities to make importers and exporters.
# -------------------------------------------------------------------------------------------------------
# This file is a part of the KrxImpExp package.
# Author: Vitaly "Kerrax" Baranov, Patrix, Shoun, Kamil "HRY" Krzyśków
# License: GPL
# -------------------------------------------------------------------------------------------------------
import os.path
import time
from typing import Callable, Dict, List, Tuple, Type, Union

import bpy
from bpy.props import BoolProperty, StringProperty
from bpy_extras.io_utils import ExportHelper, ImportHelper

from .material import loaded_texture_paths
from .preferences import KrxImpExpPreferences
from .scene import end_editmode, end_posemode
from .gui import call_message_box


class OperatorPlaceholder:
    """Used as a placeholder while the global OPERATOR isn't in use"""

    def report(self, *args, **kwargs):
        ...


OPERATOR = OperatorPlaceholder()


class OperatorSettingsHelper:
    """Class provides Operator properties/settings based on the function name"""

    bl_idname_prefix: str
    bl_label_prefix: str
    current_dict: Dict[str, Tuple[Callable, Type[bpy.types.Operator]]]
    file_exists_check: bool
    helper_mixin: Type[Union[ImportHelper, ExportHelper]]

    def __init__(self, function_name: str):
        if "imp" in function_name.lower():
            self.bl_idname_prefix = "import_scene"
            self.bl_label_prefix = "Import"
            self.current_dict = KrxImportExportManager.registered_importers
            self.current_menu = bpy.types.TOPBAR_MT_file_import
            self.file_exists_check = True
            self.helper_mixin = ImportHelper
        elif "exp" in function_name.lower():
            self.bl_idname_prefix = "export_scene"
            self.bl_label_prefix = "Export"
            self.current_dict = KrxImportExportManager.registered_exporters
            self.current_menu = bpy.types.TOPBAR_MT_file_export
            self.file_exists_check = False
            self.helper_mixin = ExportHelper
        else:
            raise NotImplementedError(
                f"{function_name} is not a valid function name and can't be registered using the manager"
            )


class KrxImportExportManager:
    """
    Class to manage the registering of custom Kerrax Import/Export operators.
    There should be no instance of this class.
    """

    registered_importers: Dict[str, Tuple[Callable, Type[bpy.types.Operator]]] = {}
    registered_exporters: Dict[str, Tuple[Callable, Type[bpy.types.Operator]]] = {}

    def __init__(self):
        raise NotImplementedError(self.__doc__)

    @classmethod
    def register(cls, function: callable, extension: str, description: str):
        """Registers either an importer or exporter operator and adds it to the appropriate menu."""

        name: str = function.__name__.split(".").pop()
        operator_settings: OperatorSettingsHelper = OperatorSettingsHelper(name)

        if name in operator_settings.current_dict:
            raise RuntimeError(f"'{name}' was already registered in the dict")

        class CustomOperator(bpy.types.Operator, operator_settings.helper_mixin):
            bl_idname: str = f"{operator_settings.bl_idname_prefix}.{name.lower()}"
            bl_label: str = f"{operator_settings.bl_label_prefix} {extension.upper()}"

            check_file: bool = operator_settings.file_exists_check
            filename_ext: str = f".{extension}"
            filter_glob: StringProperty(default=f"*.{extension}", options={"HIDDEN"})

            if operator_settings.bl_label_prefix == "Export":
                check_extension: bool = False

            def execute(self, _: bpy.types.Context):
                proceed: bool = os.path.exists(self.filepath) if self.check_file else True
                if not proceed:
                    return {"CANCELLED"}

                global OPERATOR
                placeholder = OPERATOR
                OPERATOR = self

                end_posemode()
                end_editmode()

                # Store the name of selected objects to preserve the selection
                selected_obj_names: List[str] = [obj.name for obj in bpy.context.view_layer.objects if obj.select_get()]

                addon_pref: KrxImpExpPreferences = bpy.context.preferences.addons[__package__].preferences

                if operator_settings.bl_label_prefix == "Export":
                    filepath, ext = os.path.splitext(self.filepath)
                    if not ext:
                        self.filepath = os.path.join(filepath, self.filename_ext)

                print(f"START {name}: Processing the '{self.filepath}' file")
                start_time = time.perf_counter()

                if (
                    operator_settings.bl_label_prefix == "Import"
                    and addon_pref.rescan_textures_every_time
                    and loaded_texture_paths
                ):
                    print("Clearing the loaded texture paths")
                    loaded_texture_paths.clear()

                try:
                    function(self.filepath)
                except Exception as ex:
                    call_message_box(error_message=str(ex))
                    raise

                print(f"END {name}, time {time.perf_counter() - start_time}: File processing FINISHED")

                # Restore the selection based on the stored names
                for obj in bpy.context.view_layer.objects:
                    obj.select_set(obj.name in selected_obj_names)

                OPERATOR = placeholder

                bpy.ops.outliner.orphans_purge(do_recursive=True)
                return {"FINISHED"}

        CustomOperator.__doc__ = f"Kerrax {extension} {operator_settings.bl_label_prefix}er"

        dict_empty: bool = not bool(operator_settings.current_dict)

        def menu_option_function(self, _):
            if dict_empty:
                self.layout.separator(factor=1.5)
            text = f"{description} {operator_settings.bl_label_prefix}er (.{extension})"
            self.layout.operator(CustomOperator.bl_idname, text=text)

        bpy.utils.register_class(CustomOperator)
        operator_settings.current_menu.append(menu_option_function)
        operator_settings.current_dict[name] = (menu_option_function, CustomOperator)

    @classmethod
    def unregister_all(cls):
        """Unregisters each registered importer and exporter"""

        importers = list(cls.registered_importers.keys())
        for key in importers:
            menu_option_function, operator_class = cls.registered_importers.pop(key)
            bpy.types.TOPBAR_MT_file_import.remove(menu_option_function)
            bpy.utils.unregister_class(operator_class)

        exporters = list(cls.registered_exporters.keys())
        for key in exporters:
            menu_option_function, operator_class = cls.registered_exporters.pop(key)
            bpy.types.TOPBAR_MT_file_export.remove(menu_option_function)
            bpy.utils.unregister_class(operator_class)
