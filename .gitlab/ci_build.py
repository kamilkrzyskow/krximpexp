import dataclasses
import os
import platform
import re
import sys
import traceback
import zipfile
from pathlib import Path
from typing import List

import requests


def main():
    err = run_tests()

    if err:
        sys.exit(err)

    try:
        download_packages()
        replace_asc_comment()
        create_artifacts()
        save_environment()
    except Exception:
        err = traceback.format_exc()
    finally:
        restore_asc_comment()

    if err:
        sys.exit(err)


@dataclasses.dataclass(slots=True)
class EnvironmentalVariables:
    ASSET_ANY: str
    ASSET_LINUX: str
    ASSET_MACOS: str
    ASSET_WINDOWS: str
    BUILD_JOB_ID: str
    PROJECT_NAME: str
    PROJECT_VERSION: str


def get_environment() -> EnvironmentalVariables:
    print("Set up environment")

    build_job_id = os.getenv("CI_JOB_ID")

    if not LOCAL_RUN:
        assert build_job_id is not None

    # Load __init__.pzy variables into the current locals(), so that the CI
    # is aware of the bl_info dict
    with open(ADDON_ROOT / "__init__.py", encoding="utf8") as f:
        lines = f.read().split("\n")

    init_data: List[str] = []

    append: bool = False

    for line in lines:
        if "import" in line:
            append = True

        if not line or not append or "from ." in line:
            continue

        if "def register" in line:
            break

        init_data.append(line)

    # Inject the loaded structures into locals()
    exec(" \n".join(init_data), locals())

    globals()["DEPENDENCIES"] = locals()["DEPENDENCIES"]
    assert globals()["DEPENDENCIES"] is not None
    project_name: str = "KrxImpExp"

    return EnvironmentalVariables(
        ASSET_ANY=f"{project_name}-Offline-Any.zip",
        ASSET_LINUX=f"{project_name}-Offline-Linux.zip",
        ASSET_MACOS=f"{project_name}-Offline-macOS.zip",
        ASSET_WINDOWS=f"{project_name}-Offline-Windows.zip",
        BUILD_JOB_ID=build_job_id,
        PROJECT_NAME=project_name,
        PROJECT_VERSION=".".join(map(str, locals()["bl_info"]["version"])),
    )


def run_tests() -> str:
    """Run different tests and return a string with an error message"""

    print("Running tests")

    changelog_path = ADDON_ROOT / "CHANGELOG.md"

    if not changelog_path.exists():
        return f"CHANGELOG.md was not found"

    with open(changelog_path, encoding="utf8") as file:
        content = file.read()

    current_changelog_version = f"## {ENV.PROJECT_VERSION}"
    if current_changelog_version not in content:
        return f"CHANGELOG.md doesn't contain {current_changelog_version}"

    api_releases = "https://gitlab.com/api/v4/projects/Patrix9999%2Fkrximpexp/releases"
    project_releases = get_online_resource(api_releases).json()

    release_names = []
    release_tags = []

    for release in project_releases:
        release_names.append(release["name"])
        release_tags.append(release["tag_name"])

    assert ENV.PROJECT_VERSION not in release_names
    assert ENV.PROJECT_VERSION not in release_tags

    return ""


def download_packages():
    print("Download offline packages")

    packages_dir = ADDON_ROOT / "packages"
    packages_dir.mkdir(exist_ok=True)
    dependencies = globals()["DEPENDENCIES"]

    for dependency in dependencies:
        name = dependency.module_name
        version = dependency.version

        package_url = f"https://pypi.org/pypi/{name}/{version}/json"

        package_info_response = get_online_resource(package_url)

        if package_info_response.status_code != 200:
            raise requests.ConnectionError(f"Failed to fetch package information for {name} {version}")

        data = package_info_response.json()
        files_to_download = [file["url"] for file in data["urls"] if file["url"].endswith(".whl")]

        for file_url in files_to_download:
            file_path = packages_dir / os.path.basename(file_url)

            if is_ignored_python_version(str(file_path)):
                print(f"Python version is ignored. Skipping download. {file_path}")
                continue

            if file_path.exists():
                print(f"File exists. Skipping download. {file_path}")
                continue

            file_response = get_online_resource(file_url)

            if file_response.status_code != 200:
                raise requests.ConnectionError(f"Failed to download file: {file_url}")
            else:
                print(f"Successful download: {file_url}")

            with open(file_path, "wb") as file:
                file.write(file_response.content)


def create_artifacts():
    print("Create artifacts")

    variants = {
        "Any": (".whl", ENV.ASSET_ANY),
        "Windows": ("win", ENV.ASSET_WINDOWS),
        "Linux": ("linux", ENV.ASSET_LINUX),
        "macOS": ("macos", ENV.ASSET_MACOS),
    }

    ignored_prefixes = (".git", ".gitlab", ".idea", ".vs", "gui/_temp")
    ignored_suffixes = (".zip",)
    ignored_files = (".gitignore", ".gitlab-ci.yml", "build.env", "release-CHANGELOG.md")
    ignored_wildcards = ("__pycache__", "settings", "site-packages")

    for variant, (package_part, asset) in variants.items():
        print(f"Create Offline-{variant} ZIP artifact")

        with zipfile.ZipFile(asset, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=6) as archive:
            root_dir = ENV.PROJECT_NAME
            archive.mkdir(root_dir, mode=777)

            for item in ADDON_ROOT.glob("**/*"):
                relative_to_top = item.relative_to(ADDON_ROOT).as_posix()
                if (
                    relative_to_top.startswith(ignored_prefixes)
                    or relative_to_top.endswith(ignored_suffixes)
                    or relative_to_top in ignored_files
                ):
                    continue

                if relative_to_top.endswith(".whl") and (
                    package_part not in relative_to_top or is_ignored_python_version(relative_to_top)
                ):
                    continue

                for wildcard in ignored_wildcards:
                    if wildcard in relative_to_top:
                        ignore = True
                        break
                else:
                    ignore = False

                if ignore:
                    continue

                archive.write(item, arcname=os.path.join(root_dir, relative_to_top))

        print(f"Artifact {asset} created")


def save_environment():
    if LOCAL_RUN:
        return

    with open("build.env", "w", encoding="utf8") as file:
        for field in dataclasses.fields(EnvironmentalVariables):
            file.write(f"{field.name}={getattr(ENV, field.name)}\n")


def replace_asc_comment() -> None:
    short_sha = os.getenv("CI_COMMIT_SHORT_SHA")
    timestamp = os.getenv("CI_COMMIT_TIMESTAMP")

    if not LOCAL_RUN:
        assert short_sha is not None
        assert timestamp is not None

    print(f"{short_sha=}, {timestamp=}")

    target_path = ADDON_ROOT / "KrxAscExp.py"

    with open(target_path, encoding="utf8") as file:
        content = file.read()

    for line in content.split("\n"):
        if "*COMMENT" in line:
            comment_line = line
            break
    else:
        raise ValueError("*COMMENT line not found")

    globals()["COMMENT_PLACEHOLDER"] = comment_line

    new_comment = f'"[ KrxImpExp {ENV.PROJECT_VERSION}: https://gitlab.com/Patrix9999/krximpexp ] - Commit: {short_sha} Date: {timestamp}"'

    sub = re.sub(r"\".*\"", new_comment, comment_line)
    globals()["COMMENT_SUB"] = sub
    print("sub=", sub)

    with open(target_path, "w", encoding="utf8") as file:
        file.write(content.replace(comment_line, sub))

    with open(target_path, encoding="utf8") as file:
        assert "Patrix9999" in file.read()


def restore_asc_comment() -> None:
    target_path = ADDON_ROOT / "KrxAscExp.py"

    if not globals().get("COMMENT_SUB"):
        print("No comment to restore")
        return

    with open(target_path, encoding="utf8") as file:
        content = file.read()

    with open(target_path, "w", encoding="utf8") as file:
        file.write(content.replace(globals()["COMMENT_SUB"], globals()["COMMENT_PLACEHOLDER"]))


def is_ignored_python_version(target: str) -> bool:
    for py_version in IGNORED_PYTHON_VERSIONS:
        if py_version in target:
            return True
    return False


def get_online_resource(url: str) -> requests.Response:
    user_agent = f"{ENV.PROJECT_NAME} - {ENV.PROJECT_VERSION}"
    headers = {"User-Agent": user_agent}

    return requests.get(url, headers=headers)


LOCAL_RUN: bool = False
"""Toggle to check if the script is running locally"""

ADDON_ROOT: Path = Path(__file__).parent.parent
"""Path to the root of the project"""

IGNORED_PYTHON_VERSIONS: List[str] = ["cp311", "cp312", "cp313"]
"""Version identifiers to ignore when downloading packages and creating artifacts"""

# Assume Windows as local test, no need for environmental variables for testing.
if platform.system().upper() == "WINDOWS":
    LOCAL_RUN = True

if __name__ == "__main__":
    ENV: EnvironmentalVariables = get_environment()
    main()
