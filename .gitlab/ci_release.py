from ci_build import ADDON_ROOT, EnvironmentalVariables, get_environment


def main():
    create_release_changelog()


def create_release_changelog():
    with open(ADDON_ROOT / "CHANGELOG.md", encoding="utf8") as file:
        content = file.read()

    current_version_found = False
    current_version_token = f"## {ENV.PROJECT_VERSION}"
    release_content_lines = []

    for line in content.split("\n"):
        if current_version_found:
            if "---" in line:
                break
            release_content_lines.append(line)

        if current_version_token in line:
            current_version_found = True

    with open(ADDON_ROOT / "release-CHANGELOG.md", "w", encoding="utf8") as file:
        file.write("\n".join(release_content_lines).strip())


if __name__ == "__main__":
    # This is created in and for the Release Job, therefore the BUILD_JOB_ID is incorrect here.
    ENV: EnvironmentalVariables = get_environment()
    main()
