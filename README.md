# KrxImpExp ⚙️

[Kerrax Import Export](http://krximpexp.sourceforge.net) is a open source collection of extensions made for 3d editors that adds support for [Gothic game](https://store.steampowered.com/app/39510/Gothic_II_Gold_Edition/?l=polish) 3d models.  
This project goal is to port this awesome plugin to newer [**blender**](https://www.blender.org) [API 2.80+](https://wiki.blender.org/wiki/Reference/Release_Notes/2.80/Python_API).

# Project goals 📝

- [X] **.3ds** import/export
- [X] **.mrm** import
- [X] **.msh** import
- [X] **.asc** import/export
- [X] **.zen** import

# Installation 📥

1. [Download](../../releases) the latest source code release as a zip file.
2. Extract the archive to `[drive]:/[root folder]/[Blender folder]/[version number]/scripts/addons/`  
   Example path: `C:/Program Files (x86)/Blender Foundation/3.0/scripts/addons/`
3. Rename the extracted folder from `krximpexp-[major].[minor].[patch]` to `KrxImpExp`.
4. Open up [**blender**](https://www.blender.org) and enable the extenstion
   - Goto Edit->Preferences->Add-ons
   - Search for `KrxImpExp`
   - Enable the extension by clicking on the checkbox
5. Enjoy

# Issues reporting 📌

If you find any issue with extension, report it in [project issues](../../issues).  
Add the description of the issue and steps to reproduce it if possible.

# Contributing 🤝

Any contribution to this repository is welcome.  
In order to add your own changes to the project, just open a [**merge request**](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).
