"""Add-on info and initialization of the KrxImpExp package.

Author: Vitaly (Kerrax) Baranov, Patrix, Shoun, HRY

Special thanks to Robert Gützkow. This package has been extended with the help from
https://github.com/robertguetzkow/blender-python-examples/tree/master/add_ons/install_dependencies

Licence: GPL
"""
import dataclasses
import importlib
import sys
from typing import Optional

from . import file_picker, preferences
from .impexp import KrxImportExportManager
from .Krx3dsExp import Krx3dsExpGUI
from .Krx3dsImp import Krx3dsImpGUI
from .KrxAscExp import KrxAscExpGUI
from .BatAscImp import BatAscImpGUI
from .KrxMrmImp import KrxMrmImpGUI
from .KrxMshImp import KrxMshImpGUI
from .KrxZenImp import KrxZenImpGUI
from .system import PLUGIN_SITE_PACKAGES, install_dependency, is_write_access_available

bl_info = {
    "name": "KrxImpExp",
    "description": "Plugins written by Kerrax for G1-G2a modding",
    "author": "Kerrax, Patrix, Shoun, HRY",
    "version": (2, 0, 0),
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "doc_url": "https://gitlab.com/Patrix9999/krximpexp/-/blob/main/README.md",
    "tracker_url": "https://gitlab.com/Patrix9999/krximpexp/-/issues",
    "support": "COMMUNITY",
    "category": "Import-Export",
}


@dataclasses.dataclass
class Dependency:
    module_name: str
    """Name that is used for every action by default"""
    version: str
    """Version of the package to install"""
    global_name: Optional[str] = None
    """Name that is used for the import overriding the default"""


# Declare all modules that this add-on depends on, that may need to be installed. The package and (global) name can be
# set to None, if they are equal to the module name. See import_module and ensure_and_import_module for the explanation
# of the arguments. DO NOT use this to import other parts of your Python add-on, import them as usual with an
# "import" statement.
DEPENDENCIES = (Dependency(module_name="dearpygui", version="1.8.0"),)


def register():
    """Register the plugin checking for write access and installed dependencies"""

    preferences.register()

    if not is_write_access_available():
        preferences.KrxImpExpPreferencesManager.error_message = "Write Permission Denied"

    try:
        for dependency in DEPENDENCIES:
            import_module(dependency)
    except (ModuleNotFoundError, ImportError):
        # This path is executed on fresh install, where the plugin was not installed yet.
        # Omit writing out the error, since it will only confuse the user.
        pass
    else:
        preferences.KrxImpExpPreferencesManager.dependencies_installed = True

    # Don't register other panels, operators if dependencies were not installed
    if preferences.KrxImpExpPreferencesManager.dependencies_installed:
        register_krximpexp_internals()


def register_krximpexp_internals():
    """Register KrxImpExp internal features/operators, which require installed dependencies"""

    file_picker.register()

    KrxImportExportManager.register(Krx3dsImpGUI, "3DS", "Kerrax 3D Studio Mesh")
    KrxImportExportManager.register(BatAscImpGUI, "ASC", "Shoun's ASCII Model")
    KrxImportExportManager.register(KrxMshImpGUI, "MSH", "Kerrax Compiled Mesh")
    KrxImportExportManager.register(KrxMrmImpGUI, "MRM", "Kerrax Multi-Resolution Mesh")
    KrxImportExportManager.register(KrxZenImpGUI, "ZEN", "Kerrax ZenGin World")

    KrxImportExportManager.register(Krx3dsExpGUI, "3DS", "Kerrax 3D Studio Mesh")
    KrxImportExportManager.register(KrxAscExpGUI, "ASC", "Kerrax ASCII Model")


def unregister():
    """Unregister each feature that was previously registered"""

    preferences.unregister()

    if preferences.KrxImpExpPreferencesManager.dependencies_installed:
        file_picker.unregister()
        KrxImportExportManager.unregister_all()


def import_module(dependency: Dependency, attempt_install: bool = False) -> None:
    """
    Import a module.
    :param dependency: Dependency module to import.
    :param attempt_install: Toggle to install the dependency before import.
    :raises: ImportError and ModuleNotFoundError, RuntimeError.
    """

    """
    install_dependency(module_name, module_version)
    This function replaced the original example plugin `subprocess.run` because:
    - Our explicitly installs the package into the local KrxImpExp/gui/<site-packages> directory
      without setting any environment flags and without tripping blender checks about pip,
      also doesn't pollute Blender's site-packages
    - Is compatible with 2.80 and above
    - There are additional checks for detection of operating system
    - Works on pure python executable, not on Blender's
    - Ensures pip existence with a simple command check
      while installing every dependency without importing the ensurepip module.
    """
    if attempt_install:
        result = install_dependency(dependency.module_name, dependency.version)
        if not result:
            raise RuntimeError("Installation failed")

    # Cache invalidation is required after an initially failed import.
    importlib.invalidate_caches()

    if PLUGIN_SITE_PACKAGES not in sys.path:
        sys.path.insert(0, PLUGIN_SITE_PACKAGES)

    global_name = dependency.global_name or dependency.module_name

    if global_name in globals():
        # This case occurs when the add-on was disabled and enabled again without an
        # interpreter (Blender) restart.
        importlib.reload(globals()[global_name])
    else:
        # Attempt to import the module and assign it to the globals dictionary.
        # This allows to access the module under the given name,
        # just like the regular import would.
        globals()[global_name] = importlib.import_module(dependency.module_name)

    if PLUGIN_SITE_PACKAGES in sys.path:
        sys.path.remove(PLUGIN_SITE_PACKAGES)
