# KrxImpExp - Changelog

---

## 2.0.0

List:
- Placeholder

---

## 1.1.0

**[FIXED]** Changing import scale in dialog menu will now work with ZS_ objects.  
**[FIXED]** 3DS export bug.  
**[FIXED]** Exporting ASC from .blend file.  
**[FIXED]** Importing default ASC models, bones won't be messed up.  
**[FIXED]** Parenting bone will now have assigned parent in static models.  
**[FIXED]** Duplicate materials & textures will now be discarded.  
**[UPDATED]** Imported models specular is now set to 0.0f, to avoid shiny effect.  
**[UPDATED]** Exported ASC files will now store only floats with 6 digits precision point, just like original models from Piranha Bytes does.  
**[UPDATED]** Duplicate bone objects will be discarded and instead of error, warning will be displayed in blender console.  
**[UPDATED]** All imported models scale will now be applied, to be in range <1.0, 1.0, 1.0>.  
**[UPDATED]** All html help files are now saved as utf-8, removed old html 4 tags.  
**[UPDATED]** Optimized material/uv loading with bmesh (faster loading time).  
**[UPDATED]** Optimized singleton comparison, to use `is` and `is not` instead of `==` and `!=` operators (faster loading time).  
**[ADDED]** Polish translation.

---

## 1.0.0

Initial release

---
