import bpy
from bpy.props import BoolProperty, StringProperty
from bpy.types import Operator
from bpy_extras.io_utils import ImportHelper

from .Krx3dsExp import Krx3dsExp
from .BatAscImp import BatAscImpGUI


class OT_FindFile_Filebrowser(Operator, ImportHelper):
    bl_idname = "krx.file_browser"
    bl_label = "Find File"

    filter_glob: StringProperty(options={"HIDDEN"})
    tool_mode: StringProperty(name="tool_mode", options={"HIDDEN"})
    filename_param: StringProperty(name="FILENAME_PARAM", options={"HIDDEN"})

    def execute(self, context):
        """Pick file."""
        if self.tool_mode == "3DS_MATLIB":
            Krx3dsExp(
                self.filename_param, quiet_param=False, file_picker_param=self.filepath
            )
        return {"FINISHED"}

    def cancel(self, context):
        if self.tool_mode == "3DS_MATLIB":
            Krx3dsExp(self.filename_param, quiet_param=False, file_picker_param=None)
        return None


class OT_FindFolder_Filebrowser(Operator, ImportHelper):
    bl_idname = "krx.folder_browser"
    bl_label = "Find folder"

    filter_folder: BoolProperty(default=True, options={"HIDDEN"})
    directory: StringProperty(name="Directory", options={"HIDDEN"})
    tool_mode: StringProperty(name="tool_mode", options={"HIDDEN"})
    filename_param: StringProperty(name="FILENAME_PARAM", options={"HIDDEN"})

    def execute(self, context):
        """Pick directory."""
        if self.tool_mode == "ASC_SAMPLE_SLOTS":
            BatAscImpGUI(self.filename_param, file_picker=self.directory)
        return {"FINISHED"}

    def cancel(self, context):
        if self.tool_mode == "ASC_SAMPLE_SLOTS":
            BatAscImpGUI(self.filename_param, file_picker=None)
        return None


def register():
    bpy.utils.register_class(OT_FindFolder_Filebrowser)
    bpy.utils.register_class(OT_FindFile_Filebrowser)


def unregister():
    bpy.utils.unregister_class(OT_FindFolder_Filebrowser)
    bpy.utils.unregister_class(OT_FindFile_Filebrowser)


if __name__ == "main":
    register()
