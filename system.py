import json
import os
import platform
import subprocess
import sys
from pathlib import Path
from typing import Dict, Optional

MACHINE: str = platform.machine().upper()
"""Machine/CPU architecture"""

PYTHON_VERSION: str = platform.python_version()
"""Version of the running Python executable"""

PYTHON_BIN: str = str(next((Path(sys.prefix) / "bin").glob("python*")))
"""String path to Blender's Python executable"""

SYSTEM: str = platform.system().upper()
"""System variant"""

PLUGIN_ROOT: Path = Path(os.path.dirname(os.path.realpath(__file__)))
"""Path to the root directory of the plugin"""

PLUGIN_SITE_PACKAGES: str = str(PLUGIN_ROOT / "gui" / "site-packages")
"""String path to the site-packages directory of the plugin. Used by pip to install packages."""


def is_write_access_available() -> bool:
    packages_dir: Path = Path(PLUGIN_SITE_PACKAGES)
    packages_file: Path = packages_dir / "KrxImpExp-WriteTest.log"

    try:
        # Test write access
        packages_dir.mkdir(parents=True, exist_ok=True)
        packages_file.write_text("Test", encoding="utf8")
        packages_file.unlink()
        return True
    except (PermissionError, OSError) as err:
        print(err)
        return False


def local_dearpygui_package_name(package_version: str, with_extension: bool = True) -> str:
    """
    Returns package name based on the running OS
    Example With Extension:    dearpygui-1.6.2-cp310-cp310-win_amd64.whl
    Example Without Extension: dearpygui-1.6.2-cp310-cp310-win_amd64
    """
    if package_version < "1.6.2":
        raise Exception(f"KrxImpExp: '{package_version}' dearpygui version not supported")

    python_mapping: Dict[str, str] = {
        "3.7": "cp37-cp37m",
        "3.8": "cp38-cp38",
        "3.9": "cp39-cp39",
        "3.10": "cp310-cp310",
        "3.11": "cp311-cp311",
        "3.12": "cp312-cp312",
        "3.13": "cp313-cp313",
    }

    for version in python_mapping:
        if version in PYTHON_VERSION:
            py_ver = python_mapping[version]
            break
    else:
        raise Exception(f"KrxImpExp: '{PYTHON_VERSION}' Python version not supported")

    sys_ver: str = ""
    cpu_ver: str = ""

    if SYSTEM == "WINDOWS":
        sys_ver = "win"
        if "AMD64" in MACHINE:
            cpu_ver = "amd64"
    elif SYSTEM == "LINUX":
        sys_ver = "manylinux1"
        if "X86_64" in MACHINE:
            cpu_ver = "x86_64"
    elif SYSTEM == "DARWIN":
        print(f"Warning: MacOS wasn't tested! Something might be broken!")
        sys_ver = "macosx"
        if "X86_64" in MACHINE:
            cpu_ver = "10_6_x86_64"
        elif "ARM" in MACHINE:
            cpu_ver = "11_0_arm64"

    if not sys_ver or not cpu_ver:
        raise Exception(f"KrxImpExp: '{SYSTEM}' system or '{MACHINE}' cpu version not supported")

    return f"dearpygui-{package_version}-{py_ver}-{sys_ver}_{cpu_ver}{'.whl' if with_extension else ''}"


def call_python_process(*args) -> Optional[str]:
    """Spawns Python process with arguments"""

    # Dumb ducking freetard shit, check issue #22
    # Some distros for some reason remove this ENV on wayland on RUNTIME???
    # For what duckin reason? idc anymore, nuclear solution below.
    freetards = None
    if SYSTEM == "LINUX":
        if not os.environ.get("DISPLAY", None):
            freetards = {**os.environ, "DISPLAY": ":0"}

    envs = freetards if freetards is not None else os.environ

    with subprocess.Popen(
        args=(PYTHON_BIN, *args), stdout=subprocess.PIPE, stderr=None, shell=False, env=envs
    ) as process:
        output = process.communicate()[0].decode("utf8")
        if output != "":
            return output
    return None


def install_dependency(dependency: str, version: str) -> bool:
    """Installs a given dependency.
    Calls the internal Blender Python executable to install dependency into local site-packages folder,
    so that we can use for example dearpygui.
    """

    package_path: Path = PLUGIN_ROOT / "packages" / local_dearpygui_package_name(version)
    install_target: str = str(package_path)

    if not package_path.exists():
        print(f"'{package_path}' does not exist, trying to install via network...")
        install_target = f"{dependency}=={version}"

    try:
        call_python_process("-m", "ensurepip", "--user")
        output = call_python_process("-m", "pip", "install", f"--target={PLUGIN_SITE_PACKAGES}", install_target)
    except (subprocess.SubprocessError, subprocess.CalledProcessError) as err:
        output = None
    else:
        err = None

    if version is None:
        validation_token = f"Successfully installed {dependency}"
    else:
        validation_token = f"Successfully installed {dependency}-{version}"

    print("Installation log:", "\n", output)

    if err:
        raise RuntimeError(str(err))

    if output and validation_token in output:
        return True
    else:
        print(f"Did not find the validation_token: '{validation_token}'")
        return False


class JsonHandler:
    temp_path: Path = PLUGIN_ROOT / "gui" / "_temp"

    @classmethod
    def load(cls, tool: str, dir_path: Path = None) -> Optional[Dict]:
        if dir_path is None:
            dir_path = cls.temp_path

        file_handle: Path = dir_path / f"{tool}.json"

        if not file_handle.exists():
            return None

        with file_handle.open(encoding="utf8") as file:
            return json.load(file)

    @classmethod
    def save(cls, obj, tool: str, dir_path: Path = None) -> None:
        if dir_path is None:
            dir_path = cls.temp_path

        dir_path.mkdir(parents=True, exist_ok=True)
        file_handle: Path = dir_path / f"{tool}.json"

        with file_handle.open(mode="w", encoding="utf8") as file:
            json.dump(obj, fp=file, default=vars, indent=4, sort_keys=True, ensure_ascii=False)


def load_temp_json_file(tool: str) -> Optional[Dict]:
    return JsonHandler.load(tool)


def save_temp_json_file(val, tool: str) -> None:
    JsonHandler.save(obj=val, tool=tool)
