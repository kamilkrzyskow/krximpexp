# ----------------------------------------------------
# Progress Bar & Error Box                          #
#
# ----------------------------------------------------
import os
from pathlib import Path

import bpy

from .system import PLUGIN_ROOT, call_python_process, save_temp_json_file
from .helpers import CallUIType


# Spawns external process with dearpygui interface,
# returns an output which is a dict with items from the spawned process's stdout.
# This way whenever gui crashes, it won't affect blender = no data loss for user.
def call_gui(
    file_path: str = None,
    file_type: str = None,
    ui_data: dict = None,
    message_box_mode: bool = False,
    error_message: str = None,
):
    gui_component = {
        # Imports
        CallUIType.IMPORT_STATIC_MESH: "import_asc_model.py",
        CallUIType.IMPORT_DYNAMIC_MESH: "import_asc_model.py",
        CallUIType.IMPORT_DYNAMIC_ANIM: "import_asc_dynamic_ani.py",
        CallUIType.IMPORT_MORPH_MESH: "import_morphmesh_asc.py",
        CallUIType.IMPORT_MORPH_ANIM: "import_morphmesh_asc_ani.py",
        CallUIType.IMPORT_ZEN: "import_mesh.py",
        CallUIType.IMPORT_MSH: "import_mesh.py",
        CallUIType.IMPORT_MRM: "import_mesh.py",
        CallUIType.IMPORT_3DS: "import_mesh.py",
        # Misc
        CallUIType.MESSAGE_BOX: "message_box.py",
        # Exports
        CallUIType.EXPORT_ASC: "export_asc.py",
        CallUIType.EXPORT_3DS: "export_3ds.py",

    }

    if ui_data is None:
        ui_data = {}

    gui_component_path = PLUGIN_ROOT / "gui"

    if message_box_mode:
        ui_data["error_message"] = error_message
        component = gui_component[CallUIType.MESSAGE_BOX]

        save_temp_json_file(val=ui_data, tool="MESSAGE_BOX")
        call_python_process(f"{gui_component_path}{os.sep}{component}")
        return None

    ui_data["file_path"] = file_path

    if file_type > CallUIType.MESSAGE_BOX: # Essentially acting as seperator
            save_temp_json_file(val=ui_data, tool="INPUT_EXPORT")
    else:
            save_temp_json_file(val=ui_data, tool="INPUT_IMPORT")

    save_temp_json_file(
        val={"window_width": bpy.context.window.width, "window_height": bpy.context.window.height}, tool="WINDOW"
    )

    output = call_python_process(f"{gui_component_path}{os.sep}{gui_component[file_type]}")

    return output


def call_message_box(error_message: str = "Code ?: MessageBox was called from Blender without message!"):
    call_gui(message_box_mode=True, error_message=error_message)
