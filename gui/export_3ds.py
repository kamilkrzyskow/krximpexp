import gui_values as val
from gui_system import JsonHandler, config

try:
    import dearpygui.dearpygui as dpg
except (ModuleNotFoundError, ImportError) as e:  # TODO: Selfhealing
    raise Exception(e)

from gui_generics import init_confirmation_buttons, init_file_properties
from gui_matlib import init_matlib_buttons
from gui_scale import init_scale_button
from gui_window_handling import (
    LayoutHelper,
    WindowProperties,
    define_size,
    initialize_context,
    render_window_center,
    window_handling,
)
from message_box import gui_messagebox

from helpers import ExportStatus


def krx_export(title=val.window_title_export_3ds):
    def export_callback(sender, app_data, user_data):
        unique_id = user_data
        status = ExportStatus()
        status.color_adjustment = dpg.get_value("color_adjustment_slider")

        scene_mode = dpg.get_value("scene_coordinate_system_button")
        if scene_mode == val.scene_choice_coord_system[0]:
            status.use_local_cs = False
        else:
            status.use_local_cs = True

        if dpg.get_value("rename_materials_checkbox"):
            path_input = dpg.get_value("matlibini_path_input")
            if path_input:
                status.matlib_file_path = path_input
                config.handle["MATLIB"]["matlibini_path"] = path_input
            else:
                config.handle["MATLIB"]["matlibini_path"] = ""
            unknown_materials = dpg.get_value("unknown_materials_checkbox")
            status.matlib_autorenaming = unknown_materials
            config.handle["MATLIB"]["autorenaming"] = unknown_materials

        # I think it is better to loop over all of the selected checkboxes manually than to return set of "selected_objects" that's meant ONLY to keep track of the amount
        objects_to_export = list()
        for stat in plugin_data["object_stats"]:
            object_tag = f"{unique_id}{stat['object']}"
            if (
                dpg.does_item_exist(object_tag)
                and dpg.get_value(object_tag)
                and dpg.get_item_configuration(object_tag)["enabled"]
            ):
                objects_to_export.append(stat["object"])
        status.selected_objects = objects_to_export
        status.german_chars_exception = dpg.get_value("allow_german_characters_checkbox")
        status.scale = float(dpg.get_value("scale_input"))
        status.send()

    def scene_coordinate_system_callback(sender, app_data, user_data):
        if app_data == val.scene_choice_coord_system[0]:
            dpg.set_value("scene_coordinate_system_tip", val.coord_system_desc[0])
        elif app_data == val.scene_choice_coord_system[1]:
            dpg.set_value("scene_coordinate_system_tip", val.coord_system_desc[1])

    def selected_objects_checkbox_counter_callback(sender, app_data, user_data):
        unique_id = user_data
        if isinstance(sender, str) and unique_id in sender:
            selected_object = sender.removeprefix(unique_id)
            if app_data:
                if selected_object not in selected_objects:
                    selected_objects.add(selected_object)
            else:
                if selected_object in selected_objects:
                    selected_objects.remove(selected_object)

        count = len(selected_objects)

        if count <= 0:
            dpg.configure_item("confirm_button", enabled=False)
            dpg.configure_item(
                "selected_objects_count_button", label=f"{val.selected_text}: 0. {val.tooltip_cannot_export_empty_list}"
            )
        else:
            dpg.configure_item("confirm_button", enabled=True)
            dpg.configure_item("selected_objects_count_button", label=f"{val.selected_text}: {count}")

    def selection_callback(sender, app_data, user_data):
        unique_id, switch = user_data
        selected_objects.clear()
        with dpg.mutex():
            for stat in plugin_data["object_stats"]:
                object_tag = f"{unique_id}{stat['object']}"
                if dpg.does_item_exist(object_tag) and dpg.get_item_configuration(object_tag)["enabled"]:
                    dpg.set_value(object_tag, switch)
                    if switch:
                        selected_objects.add(stat["object"])
        selected_objects_checkbox_counter_callback(sender, app_data, unique_id)

    def validate_meshes_callback(sender, app_data, user_data):
        unique_id = user_data

        with dpg.mutex():
            for stat in plugin_data["object_stats"]:
                checkbox_tag = f"{unique_id}{stat['object']}"
                tooltip_tag = f"{unique_id}{stat['object']}_tooltip"
                if dpg.does_item_exist(tooltip_tag):
                    dpg.delete_item(tooltip_tag)
                verts = stat["verts"]
                tris = stat["tris"]
                verts_in_file = stat["verts_in_file"]

                verts_tag = f"{checkbox_tag}{verts}_verts"
                tris_tag = f"{checkbox_tag}{tris}_tris"
                verts_in_file_tag = f"{checkbox_tag}{verts_in_file}_verts_in_file"

                invalid_mesh = False
                if (
                    tris > 65535
                    or verts > 65535
                    or verts_in_file > 65535
                    or stat["nonascii_names"]
                    or stat["german_chars_in_materials"]
                ):
                    invalid_mesh = True

                selected = stat["object"] in plugin_data["selected_objects"]
                selected_objects_checkbox_counter_callback(checkbox_tag, selected and not invalid_mesh, unique_id)
                dpg.configure_item(checkbox_tag, default_value=selected and not invalid_mesh)
                dpg.configure_item(checkbox_tag, enabled=True)

                if invalid_mesh:
                    if stat["german_chars_in_materials"]:
                        if app_data:  # if this is flipped, special treatment for this mesh is applied.
                            dpg.configure_item(checkbox_tag, enabled=True)
                            dpg.bind_item_theme(checkbox_tag, 0)
                        else:
                            dpg.configure_item(checkbox_tag, enabled=False)
                            dpg.bind_item_theme(checkbox_tag, "warning_yellow_theme")
                            if not dpg.does_item_exist(tooltip_tag):
                                with dpg.tooltip(checkbox_tag, tag=tooltip_tag):
                                    dpg.add_text(val.nonascii_characters_detected_text, wrap=define_size(250))

                    if verts > 65535:
                        dpg.configure_item(checkbox_tag, enabled=False)
                        dpg.bind_item_theme(verts_tag, "warning_red_theme")
                    if tris > 65535:
                        dpg.configure_item(checkbox_tag, enabled=False)
                        dpg.bind_item_theme(tris_tag, "warning_red_theme")
                    if verts_in_file > 65535:
                        dpg.configure_item(checkbox_tag, enabled=False)
                        dpg.bind_item_theme(verts_in_file_tag, "warning_red_theme")

                    if stat["nonascii_names"]:
                        dpg.configure_item(checkbox_tag, enabled=False)
                        dpg.bind_item_theme(checkbox_tag, "warning_red_theme")
                        if not dpg.does_item_exist(tooltip_tag):
                            with dpg.tooltip(checkbox_tag, tag=tooltip_tag):
                                dpg.add_text(val.nonascii_characters_detected_text, wrap=define_size(250))

    tool_mode = "EXPORT"
    scene_mode = f"{tool_mode}_3DS"
    plugin_data = JsonHandler.load(tool=f"INPUT_{tool_mode}")
    file_name = plugin_data["file_path"]

    win_width = 720
    win_height = 720

    if len(plugin_data["object_stats"]) == 0:
        # No valid export
        gui_messagebox(file_name=file_name, message=val.error_messages[4], title=val.information_text)
        exit()

    selected_objects = set()

    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(win_width), "h": define_size(win_height)}

    initialize_context()

    unique_id = f"@_@{dpg.generate_uuid()}_"
    # main window
    with dpg.window(tag="WINDOW_PANEL"):
        init_file_properties(file_name, mode=tool_mode)
        dpg.add_separator()

        dpg.add_text(val.coordinate_system_text)
        dpg.add_radio_button(
            items=val.scene_choice_coord_system,
            tag="scene_coordinate_system_button",
            callback=scene_coordinate_system_callback,
        )
        dpg.add_text(tag="scene_coordinate_system_tip", wrap=window_handler.dimension["w"] - define_size(70))
        if plugin_data["use_local_cs"]:
            dpg.set_value("scene_coordinate_system_button", val.scene_choice_coord_system[1])
            dpg.set_value("scene_coordinate_system_tip", val.coord_system_desc[1])
        else:
            dpg.set_value("scene_coordinate_system_button", val.scene_choice_coord_system[0])
            dpg.set_value("scene_coordinate_system_tip", val.coord_system_desc[0])
        dpg.add_separator()
        dpg.add_checkbox(
            label=val.allow_german_characters_text,
            tag="allow_german_characters_checkbox",
            default_value=False,
            callback=validate_meshes_callback,
            user_data=unique_id,
        )
        with dpg.tooltip("allow_german_characters_checkbox"):
            dpg.add_text(val.nonascii_characters_desc, wrap=define_size(375))

        dpg.add_separator()

        dpg.add_text(f"{val.objects_to_export_text}:")
        with dpg.group(horizontal=False):
            with dpg.child_window(tag="layout_left", height=define_size(275), width=-1, border=False):
                dpg.add_button(label="", tag="selected_objects_count_button", width=-1)
                with dpg.child_window(tag="meshes_window", height=define_size(200), width=-1):
                    dpg.add_table(tag="meshes_table", header_row=True, clipper=True)
                    dpg.add_table_column(parent="meshes_table", label=val.objects_text, width_fixed=True)
                    dpg.add_table_column(parent="meshes_table", label=val.vertices_text, width=define_size(25))
                    dpg.add_table_column(parent="meshes_table", label=val.trifaces_text, width=define_size(25))
                    dpg.add_table_column(parent="meshes_table", label=val.verts_in_file_text, width=define_size(25))
                    for stat in plugin_data["object_stats"]:
                        checkbox_tag = f"{unique_id}{stat['object']}"
                        verts = stat["verts"]
                        tris = stat["tris"]
                        verts_in_file = stat["verts_in_file"]
                        verts_tag = f"{checkbox_tag}{verts}_verts"
                        tris_tag = f"{checkbox_tag}{tris}_tris"
                        verts_in_file_tag = f"{checkbox_tag}{verts_in_file}_verts_in_file"

                        with dpg.table_row(parent="meshes_table"):
                            dpg.add_checkbox(
                                label=stat["object"],
                                tag=checkbox_tag,
                                user_data=unique_id,
                                callback=selected_objects_checkbox_counter_callback,
                            )
                            dpg.add_text(str(verts), tag=verts_tag)
                            dpg.add_text(str(tris), tag=tris_tag)
                            dpg.add_text(str(verts_in_file), tag=verts_in_file_tag)

                line = LayoutHelper()
                line.add_widget(
                    dpg.add_button(
                        label=val.select_all_text, user_data=(unique_id, True), callback=selection_callback, width=-1
                    ),
                    50,
                )
                line.add_widget(
                    dpg.add_button(
                        label=val.select_none_text, user_data=(unique_id, False), callback=selection_callback, width=-1
                    ),
                    50,
                )
                line.submit()

        dpg.add_separator()
        init_matlib_buttons(plugin_data["file_directory_matlib"])
        dpg.add_separator()
        init_scale_button(mode=tool_mode)  # button + popup window
        dpg.add_separator()

        init_confirmation_buttons(export_callback, mode=tool_mode)
        dpg.set_item_user_data("confirm_button", unique_id)

    def setup_callback():
        if plugin_data["file_directory_matlib"]:
            with dpg.mutex():
                dpg.configure_item("matlib_popup", show=True)
                dpg.configure_item("rename_materials_parameters_button", enabled=True)
                dpg.set_value("rename_materials_checkbox", True)
                render_window_center("matlib_popup")

        validate_meshes_callback("startup", False, unique_id)

    window_handling(window_handler, title, custom_callback=(setup_callback,))


if __name__ == "__main__":
    krx_export()
