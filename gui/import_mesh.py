import gui_values as val
from gui_system import JsonHandler, config

try:
    import dearpygui.dearpygui as dpg
except (ModuleNotFoundError, ImportError) as e:  # TODO: Selfhealing
    raise Exception(e)

from gui_generics import handle_slots_bones_handling_callback, init_confirmation_buttons, init_file_properties
from gui_scale import init_scale_button
from gui_window_handling import WindowProperties, define_size, initialize_context, window_handling

from helpers import ImportStatus, SceneMode


def krx_import(title=val.window_title_import_mesh):
    def import_callback():
        status = ImportStatus()
        status.color_adjustment = dpg.get_value("color_adjustment_slider")

        scene_mode = dpg.get_value("scene_radio_button")
        if scene_mode == val.scene_choice[0]:
            status.scene_mode = SceneMode.REPLACE
        elif scene_mode == val.scene_choice[1]:
            status.scene_mode = SceneMode.MERGE
        elif scene_mode == val.scene_choice_3ds_import[2]:
            status.selected_slot = dpg.get_value("scene_todo_combo_button")
            status.scene_mode = SceneMode.LINK_SLOT_TO_OBJECT
        elif scene_mode == val.scene_choice_3ds_import[3]:
            status.selected_bone = dpg.get_value("scene_todo_combo_button")
            status.scene_mode = SceneMode.LINK_BONE_TO_OBJECT

        status.scale = float(dpg.get_value("scale_input"))
        status.remove_sectored_materials = dpg.get_value("remove_sectored_materials_checkbox")
        config.handle["SCENE"]["remove_sectored_materials"] = status.remove_sectored_materials
        config.handle["SCENE"]["import_mode"] = int(status.scene_mode)
        status.send()

    scene_mode = "SIMPLE"
    tool_mode = "IMPORT"
    plugin_data = JsonHandler.load(tool="INPUT_IMPORT")
    slots = plugin_data.get("slots", None)
    bones = plugin_data.get("bones", None)
    file_name = plugin_data["file_path"]
    win_width = 600
    win_height = 340


    # Need to make shallow copy of the original values since the originals are used as a reference for callbacks
    scene_choice_3ds_import = val.scene_choice_3ds_import.copy()

    if bones and slots:
        scene_mode = "EXTENDED"
        win_height += 90

    if slots and scene_mode == "SIMPLE":
        scene_mode = "EXTENDED"
        scene_choice_3ds_import.pop(3)
        win_height += 60
    if bones and scene_mode == "SIMPLE":
        scene_mode = "EXTENDED"
        scene_choice_3ds_import.pop(2)
        win_height += 60

    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(win_width), "h": define_size(win_height)}

    initialize_context()
    # main window
    with dpg.window(tag="WINDOW_PANEL"):
        init_file_properties(file_name)
        dpg.add_separator()

        if scene_mode == "SIMPLE":
            with dpg.group():
                dpg.add_text(val.scene_todo_text)
                dpg.add_radio_button(items=val.scene_choice, default_value=val.scene_choice[0], tag="scene_radio_button")
        else:
            with dpg.group():
                dpg.add_text(val.scene_todo_text)
                dpg.add_radio_button(
                    items=scene_choice_3ds_import,
                    default_value=scene_choice_3ds_import[0],
                    callback=handle_slots_bones_handling_callback,
                    user_data=(plugin_data,),
                    tag="scene_radio_button",
                )
                with dpg.group(horizontal=True):
                    dpg.add_text(show=False, tag="scene_todo_slot_bone_text")
                    dpg.add_combo(width=-1, show=False, tag="scene_todo_combo_button")

        dpg.add_separator()
        dpg.add_checkbox(
            label=val.sectored_materials_text,
            tag="remove_sectored_materials_checkbox",
            default_value=config.handle["SCENE"]["remove_sectored_materials"],
        )
        with dpg.tooltip("remove_sectored_materials_checkbox"):
            dpg.add_text(val.sectored_materials_desc, wrap=define_size(250))
        dpg.add_separator()
        init_scale_button(mode=tool_mode)
        dpg.add_separator()

        init_confirmation_buttons(import_callback, mode=tool_mode)

    window_handling(window_handler, title)


if __name__ == "__main__":
    krx_import()
