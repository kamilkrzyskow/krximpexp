from dataclasses import dataclass, field
from typing import Dict

import dearpygui.dearpygui as dpg
from gui_system import config, SYSTEM, GUI_ROOT, JsonHandler

dpi_multiplier = float(config.handle["OPTIONS"]["dpi_multiplier"])
window_data = JsonHandler.load(tool="WINDOW")


def define_size(x):
    return x * dpi_multiplier


default_window_dimension = {"w": define_size(600), "h": define_size(300)}

default_modal_size = {"w": define_size(550), "h": define_size(200)}

default_button_size = {"w": define_size(120), "h": define_size(20)}

FONT_SCALE = 2  # Global font scale used for making it look better - improves it on Unix based and MacOS systems


def save_window_pos():
    if not config.handle["OPTIONS"]["first_run"]:
        pos = dpg.get_viewport_pos()
        config.handle["OPTIONS"]["window_pos_x"] = pos[0]
        config.handle["OPTIONS"]["window_pos_y"] = pos[1]


generic_modal_cfg = {
    "modal": True,
    "show": False,
    "no_resize": True,
    "no_move": True,
    "no_title_bar": True,
    "width": default_modal_size["w"],
    "height": default_modal_size["h"],
}


class LayoutHelper:
    def __init__(self):
        self.table_id = dpg.add_table(header_row=False, policy=dpg.mvTable_SizingStretchProp)
        self.stage_id = dpg.add_stage()
        dpg.push_container_stack(self.stage_id)

    def add_widget(self, uuid, percentage):
        dpg.add_table_column(init_width_or_weight=percentage / 100.0, parent=self.table_id)
        dpg.set_item_width(uuid, -1)

    def submit(self):
        dpg.pop_container_stack()
        with dpg.table_row(parent=self.table_id):
            dpg.unstage(self.stage_id)


@dataclass
class WindowProperties:
    dimension: Dict[str, int] = field(default_factory=lambda: default_window_dimension)
    screen_size: Dict[str, int] = field(
        default_factory=lambda: {"w": window_data["window_width"], "h": window_data["window_height"]}
    )
    modal_size: Dict[str, int] = field(default_factory=lambda: default_button_size)


def render_window_center(item):
    if dpg.does_item_exist(item):
        main_width = dpg.get_viewport_client_width()
        main_height = dpg.get_viewport_client_height()
        child_width = dpg.get_item_width(item)
        child_height = dpg.get_item_height(item)
        dpg.set_item_pos(item, [(main_width - child_width) // 2, (main_height - child_height) // 2])


# https://github.com/hoffstadt/DearPyGui/issues/2034 << this was fixed and now we need to use this ;/
def get_id(tag: str):
    return dpg.get_alias_id(tag)


def init_theming():
    # Global theming, do not put item specific themes here
    with dpg.theme() as disabled_theme:
        with dpg.theme_component(dpg.mvInputText, enabled_state=False):
            dpg.add_theme_color(dpg.mvThemeCol_Text, [180, 180, 180])

        with dpg.theme_component(dpg.mvInputFloat, enabled_state=False):
            dpg.add_theme_color(dpg.mvThemeCol_Text, [180, 180, 180])

        with dpg.theme_component(dpg.mvInputInt, enabled_state=False):
            dpg.add_theme_color(dpg.mvThemeCol_Text, [180, 180, 180])

        with dpg.theme_component(dpg.mvCheckbox, enabled_state=False):
            dpg.add_theme_color(dpg.mvThemeCol_CheckMark, [100, 100, 100])

        with dpg.theme_component(dpg.mvButton, enabled_state=False):
            dpg.add_theme_color(dpg.mvThemeCol_Text, [200, 200, 200])

    dpg.add_theme(tag="warning_red_theme")
    with dpg.theme_component(dpg.mvAll, parent="warning_red_theme", enabled_state=False):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 0, 0), category=dpg.mvThemeCat_Core)
    with dpg.theme_component(dpg.mvAll, parent="warning_red_theme"):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 0, 0), category=dpg.mvThemeCat_Core)

    dpg.add_theme(tag="warning_yellow_theme")
    with dpg.theme_component(dpg.mvAll, parent="warning_yellow_theme", enabled_state=False):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 255, 0), category=dpg.mvThemeCat_Core)
    with dpg.theme_component(dpg.mvAll, parent="warning_yellow_theme"):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 255, 0), category=dpg.mvThemeCat_Core)
    dpg.bind_theme(disabled_theme)


def initialize_context():
    try:
        dpg.destroy_context()
    finally:
        dpg.create_context()
        dpg.setup_dearpygui()
        init_theming()


def window_handling(window, title="Default Window", custom_callback: callable = None):
    def register_fonts(path_to_fonts=GUI_ROOT / "fonts"):
        font_path_noto = path_to_fonts / "NotoSans-Regular.ttf"

        with dpg.font_registry():  # We have to include new font since default one is bad
            with dpg.font(font_path_noto, define_size(19) * FONT_SCALE, tag="default_font"):
                dpg.add_font_range_hint(dpg.mvFontRangeHint_Default)
                dpg.add_font_range_hint(dpg.mvFontRangeHint_Cyrillic)
                dpg.add_font_range(0x0104, 0x017C)
                dpg.add_font_chars([0x25BC, 0x25BA])
            dpg.set_global_font_scale(1 / FONT_SCALE)
            dpg.bind_font("default_font")

    def start_callback():
        # Tip i got from DPG Discord, makes the font look crisper on highDPI screens on windows 10
        if SYSTEM == "WINDOWS":
            import ctypes

            ctypes.windll.shcore.SetProcessDpiAwareness(2)
        if custom_callback:
            for callback in custom_callback:
                callback()

    x_pos = (window.screen_size["w"] - window.dimension["w"]) // 2
    y_pos = (window.screen_size["h"] - window.dimension["h"]) // 2

    register_fonts()

    dpg.create_viewport(title=title, always_on_top=False)
    dpg.set_primary_window("WINDOW_PANEL", True)
    if config.handle["OPTIONS"]["first_run"]:
        config.handle["OPTIONS"]["first_run"] = False
        config.handle["OPTIONS"]["window_pos_x"] = x_pos
        config.handle["OPTIONS"]["window_pos_y"] = y_pos
    dpg.configure_viewport(
        item=0,
        autosize=False,
        no_collapse=True,
        fullscreen=False,
        resizable=False,
        no_close=True,
        vsync=True,
        min_width=0,
        min_height=0,
        width=window.dimension["w"],
        height=window.dimension["h"],
        x_pos=config.handle["OPTIONS"]["window_pos_x"],
        y_pos=config.handle["OPTIONS"]["window_pos_y"],
        # x_pos=10000,
        # y_pos=10000,
    )

    dpg.show_viewport()
    dpg.set_exit_callback(save_window_pos)
    while dpg.is_dearpygui_running():
        dpg.render_dearpygui_frame()
        dpg.set_frame_callback(frame=4, callback=start_callback)
    dpg.start_dearpygui()

    dpg.destroy_context()
    config.save()
